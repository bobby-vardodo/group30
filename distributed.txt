-------------------------
|       Features        |
-------------------------

When in a collection, distributed databases are logically interrelated with each other, and they often represent a single logical database. 
With distributed databases, data is physically stored across multiple sites and independently managed. ... Distributed query processing.

-----------------------------
|       Pros and Cons       |
-----------------------------

https://it.toolbox.com/blogs/craigborysowich/pros-cons-of-distributed-databases-051507

Strengths of Distributed Databases

Data communications costs are lower with distributed databases because processing can be performed locally.  Since storage unit costs are typically less expensive than telecommunications costs, providing access to distributed data may be less expensive than providing access to centralized data.
Distributed databases can take advantage of benefits of a centralized database (in particular, data sharing).
Distributed environments may be less affected by a breakdown than a centralized system.  If one computer breaks down, it may be possible to perform the functions on another computer in the organization.  This depends on the particular type of topology and software controlling the communications as well as on the nature of the distributed data.

Weaknesses of Distributed Databases

Distributed databases introduce data management complexities for issues relating to data concurrency and integrity.  Data replicated at several sites may lead to inconsistencies if the data is modified.  Distributing data, processing, and computing staff may lead to local "patches" to satisfy local user needs.
There may be significant technical difficulties in implementing a distributed system.  Complex networking software is needed to control data communications.  Communication overhead may be quite high.

https://www.quora.com/What-are-the-advantages-and-disadvantages-of-distributed-databases
Advantages:

Loss of a single database does not cause all data to be lost.
User interaction is primarily with a local database resulting in higher access speeds. This yields faster reporting, and quicker Queries.
Cheaper operation resulting from taking advantage of off-peak rates of communications between the Master and Satellite databases syncing on scheduled access.
The Satellite databases contains local changes until the Master database acknowledges that it has updated these changes. This prevents accidental loss of local data during transmission
A significant improvement in Master database performance, as it doesn’t have to deal with the Satellite database changes constantly, i.e. You simply move your Satellite transactional load to a manageable schedule leaving more time for other operations.

Disadvantages:

Complexity, the Distributed database architecture is more demanding in terms of design, troubleshooting and administration.
There are problems in accessing Satellite data Ad Hoc (anytime) from the Master database. This can usually is done in accordance with the scheduled syncs. Of course there is usually a manual override where syncing can be made to occur.
Many complex routines have to be developed to insure that data synchronization is working correctly.
Delays will exist between the Master and Satellite data footprints, where many reports generated by the Satellite database are not current with the Master’s data. This problem has to be evaluated during the design of the sync frequency.