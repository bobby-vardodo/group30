Main memory databases are faster than disk-optimized databases since the internal optimization algorithms are simpler and execute fewer CPU instructions. 
Accessing data in memory eliminates seek time when querying the data, which provides faster and more predictable performance than disk.

Redis would be a good example of a Memory database. 

Redis is an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker. 
It supports data structures such as strings, hashes, lists, sets, sorted sets with range queries, bitmaps, hyperloglogs and geospatial indexes with radius queries.

Redis in the NoSQL ecosystem. ... Redis (REmote DIctionary Server) is key-value in-memory database storage that also supports disk storage for persistence.
It supports several data types: Strings, Hashes, Lists, Sets and Sorted Sets; implements publish/subscribe messaging paradigm and has transactions.

Memcached (pronunciation: mem-cashed, mem-cash-dee) is a general-purpose distributed memory caching system. 
It is often used to speed up dynamic database-driven websites by caching data and objects in RAM to reduce the number of times an external data source (such as a database or API) must be read.

Redis has lots of features and is very fast, but completely limited to one core as it is based on an event loop. We use both. Memcached is used for caching objects, primarily reducing read load on the databases. 
Redis is used for things like sorted sets which are handy for rolling up time-series data.