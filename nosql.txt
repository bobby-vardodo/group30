-------------------------
|       Features        |
-------------------------

The 5 Features to Look for in a NoSQL Database
Multi-Model. Where relational databases require data to be put into tables and columns to be accessed and analyzed, the various data model capabilities of NoSQL databases make them extremely flexible when it comes to handling data. ...
Easily Scalable. ...
Flexible. ...
Distributed. ...
Zero Downtime.

-------------------------
|       Pros / Cons     |
-------------------------

For years the rational data base has been the dominating database management model. 
However, nowadays non-rational NoSQL or cloud databases are gaining popularity as the best database management alternative model. 
Here are the pros and cons of NoSQL.


List Of Pros of NoSQL
1. Flexible Scalability
Unlike rational database management model that is difficult to scale out when it come to commodity clusters NoSQL models make use of new nodes which makes them transparent for expansion. The model is designed to be used even with low cost hardwares. In this current world where outward scalability is replacing upwards scalability, NoSQL models are the better option.

2. Stores Massive Amounts Of Data
Given the fact that transaction rates are rising due to recognition, huge volumes of data need to be stored. While rational models have grown to meet this need it is illogical to use such models to store such large volumes of data. However these volumes can easily be handled by NoSQL models

3. Database Maintenance
The best rational models need the service of an expert to design, install and maintain. However, NoSQL models need much less expert management as it already has auto repair and data distribution capabilities, fewer administration and turning requirements as well as simplified data designs.

4. Economical
Rational models require expensive proprietary servers and storage systems whereas NoSQL models are easy and cheap to install. This means that more data can be processed and stored at a very minimal cost.


List Of Cons of NoSQL
1. Not Mature
Rational models have been around for some time now compared to NoSQL models and as a result they have grown to be more functional and stable systems over the years.

2. Less Support
Every business should be reassured that in case a key function in their database system fails, they will have unlimited competent support any time. All rational model vendors have gone the extra mile to provide this assurance and made it sure that their support is available 24 hours which is not a step yet guaranteed by NoSQL vendors.

3. Business Analytics And Intelligence
NoSQL models were created because of the modern-day web 2.0 web applications in mind. And because of this, most NoSQL features are focused to meeting these demands ignoring the demands of apps made without these characteristics hence end up offering fewer analytic features for normal web apps.

Any businesses looking to implement NoSQL model needs to do it with caution, remembering the above mentioned pros and cons they posse in contrast to their rational opposites.