## GROUP 30 - Databases Project

Databases are commonly used in computer systems for information management. 

In this project, you need to explore and summarise the various categories of databases used in computer systems, 
such as NoSQL databases, relational databases, distributed databases etc. 
and their representative examples. 

You need also to consider their features such as their suitability for different 
data types, platforms, the methods for securing their data.